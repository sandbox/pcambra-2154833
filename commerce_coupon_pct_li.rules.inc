<?php

/**
 * Implements hook_rules_action_info().
 */
function commerce_coupon_pct_li_rules_action_info() {
  $actions = array();
  $actions['commerce_coupon_pct_li_apply_pct_coupon'] = array(
    'label' => t('Apply a percentage coupon to a product a limited number of times'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('commerce order'),
      ),
      'coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
      ),
      'limited_items' => array(
        'type' => 'integer',
        'label' => t('Limit the times this coupon is applied per line item'),
      ),
      'component_name' => array(
        'type' => 'text',
        'label' => t('Price component type'),
        'description' => t('Price components track changes to prices made during the price calculation process, and they are carried over from the unit price to the total price of a line item. When an order total is calculated, it combines all the components of every line item on the order. When the unit price is altered by this action, the selected type of price component will be added to its data array and reflected in the order total display when it is formatted with components showing. Defaults to base price, which displays as the order Subtotal.'),
        'options list' => 'commerce_price_component_titles',
        'default value' => 'base_price',
      ),
      'round_mode' => array(
        'type' => 'integer',
        'label' => t('Price rounding mode'),
        'description' => t('Round the resulting price amount after performing this operation.'),
        'options list' => 'commerce_round_mode_options_list',
        'default value' => COMMERCE_ROUND_HALF_UP,
      ),
    ),
    'base' => 'commerce_coupon_pct_li_apply_pct_coupon',
    'group' => t('Commerce coupon pct line item'),
  );

  return $actions;
}

/**
 * Implements hook_rules_condition_info()
 */
function commerce_coupon_pct_li_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_coupon_pct_li_order_has_referenced_product'] = array(
    'label' => t('Order contains a product that is referenced by the coupon.'),
    'group' => t('Commerce coupon pct line item'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('Specifies the order to check.'),
      ),
      'commerce_coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
        'description' => t("Specifies the coupon with the product reference field."),
      ),
      'field' => array(
        'type' => 'text',
        'label' => t("Field"),
        'description' => t("Specifies the product reference field that should be used."),
        'options list' => 'commerce_coupon_pct_li_product_field_options',
        'restriction' => 'input',
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_coupon_pct_li_order_has_referenced_product',
    ),
  );

  return $conditions;
}

function commerce_coupon_pct_li_apply_pct_coupon($order, $coupon, $limit_qty, $component_name, $round_mode) {
  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);
  $fields = $coupon_wrapper->getPropertyInfo();
  if ($coupon->is_active == TRUE && $coupon->type == 'commerce_coupon_pct_li'
    && isset($fields['commerce_coupon_percent_amount']) && $coupon_wrapper->commerce_coupon_percent_amount->value() > 0) {
    $rate = $coupon_wrapper->commerce_coupon_percent_amount->value();
    if ($rate > 1) {
      // Ensure that the rate is never bigger then 100%
      $rate = $rate / 100;
    }
    else {
      return;
    }

    $amount = 0;
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $limited_products = $coupon_wrapper->commerce_coupon_pct_li_prodref->value();
    foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
      if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
        if (empty($limited_products) || in_array($line_item_wrapper->commerce_product->value(), $limited_products)) {
          // We might need to apply this coupon several times, so we calculate the min
          // of the limit and the actual quantity in the cart.
          $limit_qty = min(array($limit_qty, $line_item_wrapper->quantity->value()));
          $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
          $amount += commerce_round($round_mode, $unit_price['amount'] * $rate) * $limit_qty;
        }
      }
    }
    if ($amount) {
      $unit_price = commerce_price_wrapper_value($order_wrapper, 'commerce_order_total', TRUE);
      module_load_include('rules.inc', 'commerce_coupon');
      commerce_coupon_action_create_coupon_line_item($coupon, $order, $amount, $component_name, $unit_price['currency_code']);
    }
  }
}

/**
 * Rule condition callback for checking if an order contains products referenced
 * by a coupon.
 *
 * @return
 *   TRUE if $order contains a product referenced by $field on $commerce_coupon;
 *   FALSE otherwise.
 */
function commerce_coupon_pct_li_order_has_referenced_product($order, $commerce_coupon, $field) {

  // Build list of referenced product IDs.
  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $commerce_coupon);

  if (isset($coupon_wrapper->{$field})) {
    $products = array();
    foreach ($coupon_wrapper->{$field} as $product) {
      $id = $product->product_id->value();
      $products[$id] = $id;
    }

    // Go through line items looking for a match.
    if ($products) {
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
      foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
        if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
          $id = $line_item_wrapper->commerce_product->product_id->value();
          if (isset($products[$id])) {
            // At least one product matches.
            return TRUE;
          }
        }
      }
    }
  }

  // Product wasn't found.
  return FALSE;
}

/**
 * Options list callback that returns a list of fields that are attached to the
 * entity commerce_coupon and could reference a product.
 */
function commerce_coupon_pct_li_product_field_options() {

  $fields = array();

  foreach (field_info_fields() as $field) {

    // Check if the field is attached to the entity commerce_coupon.
    if (isset($field['bundles']['commerce_coupon'])) {

      // The field is considered a valid reference field if *any* of the
      // following are true:
      // a) it's a product reference field;
      // b) it's an entity reference field set to target commerce_product
      //    entities;
      // c) it's an entity reference field that uses a View for its source.
      $type = $field['type'];

      $valid_product_reference = ($type == 'commerce_product_reference');
      $valid_entity_reference = ($type == 'entityreference') && in_array($field['settings']['target_type'], array('commerce_product', 'view'));

      if ($valid_product_reference || $valid_entity_reference) {
        $fields[$field['field_name']] = $field['field_name'];
      }
    }
  }

  return $fields;
}

