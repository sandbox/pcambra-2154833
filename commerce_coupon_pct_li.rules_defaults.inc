<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_coupon_pct_li_default_rules_configuration() {
  $items = array();
  $items['commerce_coupon_pct_li_add_ltd_pct_coupon'] = entity_import('rules_config', '{ "commerce_coupon_pct_li_add_ltd_pct_coupon" : {
      "LABEL" : "Add line item coupon with percentage amount",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_coupon_pct_li", "commerce_coupon" ],
      "ON" : { "commerce_coupon_redeem" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "coupon:type" ], "value" : "commerce_coupon_pct_li" } },
        { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_percent_amount" } },
        { "NOT AND" : [
            { "data_is_empty" : { "data" : [ "coupon:commerce-coupon-percent-amount" ] } }
          ]
        },
        { "entity_has_field" : {
            "entity" : [ "commerce_order" ],
            "field" : "commerce_coupon_order_reference"
          }
        },
        { "data_is" : { "data" : [ "coupon:is-active" ], "op" : "=", "value" : true } },
        { "data_is" : {
            "data" : [ "coupon:commerce-coupon-percent-amount" ],
            "op" : "\u003E",
            "value" : "0"
          }
        },
        { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_limit_by_order" } }
      ],
      "DO" : [
        { "list_add" : {
            "list" : [ "commerce-order:commerce-coupon-order-reference" ],
            "item" : [ "coupon" ],
            "unique" : 1
          }
        },
        { "commerce_coupon_pct_li_apply_pct_coupon" : {
            "commerce_order" : [ "commerce_order" ],
            "coupon" : [ "coupon" ],
            "limited_items" : [ "coupon:commerce-coupon-limit-by-order" ],
            "component_name" : "commerce_coupon_pct_li_lct01",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');

  $items['commerce_coupon_pct_li_validate_refererenced_products'] = entity_import('rules_config', '{ "commerce_coupon_pct_li_validate_refererenced_products" : {
      "LABEL" : "Coupon percentage line item validation: Check the referenced products",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_coupon_pct_li", "commerce_coupon" ],
      "ON" : { "commerce_coupon_validate" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_pct_li_prodref" } },
        { "NOT data_is_empty" : { "data" : [ "coupon:commerce-coupon-pct-li-prodref" ] } },
        { "NOT commerce_coupon_pct_li_order_has_referenced_product" : {
            "commerce_order" : [ "commerce_order" ],
            "commerce_coupon" : [ "coupon" ],
            "field" : "commerce_coupon_pct_li_prodref"
          }
        }
      ],
      "DO" : [ { "commerce_coupon_action_is_invalid_coupon" : [] } ]
    }
  }');


  return $items;
}
